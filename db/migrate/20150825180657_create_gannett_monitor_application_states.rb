class CreateGannettMonitorApplicationStates < ActiveRecord::Migration
  def change
    create_table :gannett_monitor_application_states do |t|
      t.string :name

      t.timestamps
    end
  end
end
