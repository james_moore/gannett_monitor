require 'test_helper'

module GannettMonitor
  class ApplicationStatesControllerTest < ActionController::TestCase
    setup do
      @application_state = application_states(:one)
    end

    test "should get index" do
      get :index
      assert_response :success
      assert_not_nil assigns(:application_states)
    end

    test "should get new" do
      get :new
      assert_response :success
    end

    test "should create application_state" do
      assert_difference('ApplicationState.count') do
        post :create, application_state: { name: @application_state.name }
      end

      assert_redirected_to application_state_path(assigns(:application_state))
    end

    test "should show application_state" do
      get :show, id: @application_state
      assert_response :success
    end

    test "should get edit" do
      get :edit, id: @application_state
      assert_response :success
    end

    test "should update application_state" do
      patch :update, id: @application_state, application_state: { name: @application_state.name }
      assert_redirected_to application_state_path(assigns(:application_state))
    end

    test "should destroy application_state" do
      assert_difference('ApplicationState.count', -1) do
        delete :destroy, id: @application_state
      end

      assert_redirected_to application_states_path
    end
  end
end
