require_dependency "gannett_monitor/application_controller"

module GannettMonitor
  class ApplicationStatesController < ApplicationController
    before_action :set_application_state, only: [:show, :edit, :update, :destroy]

    # GET /application_states
    def index
      @application_states = ApplicationState.all
    end

    # GET /application_states/1
    def show
    end

    # GET /application_states/new
    def new
      @application_state = ApplicationState.new
    end

    # GET /application_states/1/edit
    def edit
    end

    # POST /application_states
    def create
      @application_state = ApplicationState.new(application_state_params)

      if @application_state.save
        redirect_to @application_state, notice: 'Application state was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /application_states/1
    def update
      if @application_state.update(application_state_params)
        redirect_to @application_state, notice: 'Application state was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /application_states/1
    def destroy
      @application_state.destroy
      redirect_to application_states_url, notice: 'Application state was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_application_state
        @application_state = ApplicationState.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def application_state_params
        params.require(:application_state).permit(:name)
      end
  end
end
