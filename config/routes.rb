GannettMonitor::Engine.routes.draw do
  resources :application_states
  root to: "application_states#index"
end
